#!/usr/bin/ruby
require 'sinatra'
require 'json'

get '/hello' do
  results = "{\"text\":\"Hello World!\", \"time\": \"#{Time.new}\"}"
end

get '/' do
  status, headers, body = call env.merge("PATH_INFO" => '/hello')
  [status, headers, body.map(&:upcase)]
end
